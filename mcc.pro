
TEMPLATE = lib
TARGET = mcc
INCLUDEPATH += . src
QT += widgets sql
DEFINES += LIBMCC_LIBRARY
#QMAKE_CXXFLAGS += -mmmx -msse2 -msse3 -msse4.1 -m3dnow -std=gnu++11

# Input
HEADERS += src/Application.h \
		   src/ButtonsLine.h \
		   src/ColorDisplay.h \
		   src/ColorPicker.h \
		   src/ComboBox.h \
		   src/Dialog.h \
		   src/DialogConnectBase.h \
		   src/DialogSettingsBase.h \
		   src/FSDialog.h \
		   src/Frame.h \
		   src/Info.h \
		   src/Label.h \
		   src/MainWindowBase.h \
		   src/Settings.h \
		   src/Version.h \
		   src/libmcc_global.h

SOURCES += src/Application.cpp \
		   src/ButtonsLine.cpp \
		   src/ColorDisplay.cpp \
		   src/ColorPicker.cpp \
		   src/ComboBox.cpp \
		   src/Dialog.cpp \
		   src/DialogConnectBase.cpp \
		   src/DialogSettingsBase.cpp \
		   src/FSDialog.cpp \
		   src/Frame.cpp \
		   src/Info.cpp \
		   src/Label.cpp \
		   src/MainWindowBase.cpp \
		   src/Settings.cpp \
		   src/Version.cpp

RESOURCES += qrc/images.qrc \

TRANSLATIONS = translations/mcc_ru.ts

QMAKE_POST_LINK = lupdate $${_PRO_FILE_}		# обновить переводы

