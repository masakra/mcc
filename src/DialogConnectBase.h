/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
 ┃                             ________________                                ┃
 ┃ _______ ________________    ___  /__(_)__  /______________ ____________  __ ┃
 ┃ __  __ `__ \  ___/  ___/    __  /__  /__  __ \_  ___/  __ `/_  ___/_  / / / ┃
 ┃ _  / / / / / /__ / /__      _  / _  / _  /_/ /  /   / /_/ /_  /   _  /_/ /  ┃
 ┃ /_/ /_/ /_/\___/ \___/      /_/  /_/  /_.___//_/    \__,_/ /_/    _\__, /   ┃
 ┃                                                                   /____/    ┃
 ┠─────────────────────────────────────────────────────────────────────────────┨
 ┃ Copyright © 2016, Sergey N Chursanov (masakra@mail.ru)                      ┃
 ┃ All rights reserved.                                                        ┃
 ┃                                 BSD license                                 ┃
 ┃                                                                             ┃
 ┃ Redistribution and use in source and binary forms, with or without          ┃
 ┃ modification, are permitted provided that the following conditions are met: ┃
 ┃                                                                             ┃
 ┃ 1. Redistributions of source code must retain the above copyright notice,   ┃
 ┃ this list of conditions and the following disclaimer.                       ┃
 ┃                                                                             ┃
 ┃ 2. Redistributions in binary form must reproduce the above copyright        ┃
 ┃ notice, this list of conditions and the following disclaimer in the         ┃
 ┃ documentation and/or other materials provided with the distribution.        ┃
 ┃                                                                             ┃
 ┃ THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" ┃
 ┃ AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE   ┃
 ┃ IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE  ┃
 ┃ ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE   ┃
 ┃ LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR         ┃
 ┃ CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF        ┃
 ┃ SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS    ┃
 ┃ INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN     ┃
 ┃ CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)     ┃
 ┃ ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE  ┃
 ┃ POSSIBILITY OF SUCH DAMAGE.                                                 ┃
 ┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
#pragma once

#include <QDialog>

class QCheckBox;
class QLabel;
class QLineEdit;
class QToolButton;

#define DIALOG_CONNECT_BASE_GROUP "DialogConnect"
#define DIALOG_CONNECT_BASE_AUTOLOGIN "autologin"

/** \brief Базовый класс для диалогов подключения к БД
 *
 * Удаляется при закрытии
 */
class DialogConnectBase : public QDialog
{
	Q_OBJECT

	private:
		QPixmap m_pixmapBackground;

		QString m_driver;

		QStringList m_messages;

		void createWidgets();

		QLineEdit * m_editUsername,
				  * m_editPassword,
				  * m_editHost,
				  * m_editPort,
				  * m_editDatabase;

		QToolButton * m_buttonServer;

		QCheckBox * m_checkAutologin;

		bool m_autologin;

		QLabel * m_labelInfo,
			   * m_labelHost,
			   * m_labelPort,
			   * m_labelDatabase;

		QPushButton * m_buttonConnect;

		void setInfo();

		QWidget * load();

		void save() const;

	private Q_SLOTS:
		void checkFields( const QString & text );
		void toggleServerFieldsVisible( bool e );
		void connectToDatabase();

	protected:
		void paintEvent( QPaintEvent * event );

		void setVersionPos( int x, int y );

		QColor m_colorVersion,
			   m_colorMessage,
			   m_colorLastMessage;

		QPoint m_versionPos;

	public:
		DialogConnectBase( QWidget * parent, const QString & driver,
				const QString & background = QLatin1String(":/dialog_connect_bg_default.png") );

		void enableAutologin( bool e );

	public Q_SLOTS:
		void addMessage( const QString & message );
};

