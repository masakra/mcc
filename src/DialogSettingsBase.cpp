/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
 ┃                             ________________                                ┃
 ┃ _______ ________________    ___  /__(_)__  /______________ ____________  __ ┃
 ┃ __  __ `__ \  ___/  ___/    __  /__  /__  __ \_  ___/  __ `/_  ___/_  / / / ┃
 ┃ _  / / / / / /__ / /__      _  / _  / _  /_/ /  /   / /_/ /_  /   _  /_/ /  ┃
 ┃ /_/ /_/ /_/\___/ \___/      /_/  /_/  /_.___//_/    \__,_/ /_/    _\__, /   ┃
 ┃                                                                   /____/    ┃
 ┠─────────────────────────────────────────────────────────────────────────────┨
 ┃ Copyright © 2016, Sergey N Chursanov (masakra@mail.ru)                      ┃
 ┃ All rights reserved.                                                        ┃
 ┃                                 BSD license                                 ┃
 ┃                                                                             ┃
 ┃ Redistribution and use in source and binary forms, with or without          ┃
 ┃ modification, are permitted provided that the following conditions are met: ┃
 ┃                                                                             ┃
 ┃ 1. Redistributions of source code must retain the above copyright notice,   ┃
 ┃ this list of conditions and the following disclaimer.                       ┃
 ┃                                                                             ┃
 ┃ 2. Redistributions in binary form must reproduce the above copyright        ┃
 ┃ notice, this list of conditions and the following disclaimer in the         ┃
 ┃ documentation and/or other materials provided with the distribution.        ┃
 ┃                                                                             ┃
 ┃ THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" ┃
 ┃ AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE   ┃
 ┃ IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE  ┃
 ┃ ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE   ┃
 ┃ LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR         ┃
 ┃ CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF        ┃
 ┃ SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS    ┃
 ┃ INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN     ┃
 ┃ CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)     ┃
 ┃ ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE  ┃
 ┃ POSSIBILITY OF SUCH DAMAGE.                                                 ┃
 ┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

#include "DialogSettingsBase.h"

#include <QtWidgets>

#include "ButtonsLine.h"

#define GROUP "DialogSettings"
#define STACK_INDEX "StackIndex"

DialogSettingsBase::DialogSettingsBase( QWidget * parent )
	: Dialog( parent )
{
	setWindowTitle( tr("Settings") );
	setWindowIcon( QIcon(":/avcc_gear_16.png") );
	setAttribute( Qt::WA_DeleteOnClose );
	setWindowFlags( Qt::WindowStaysOnTopHint );

	createWidgets();
}

DialogSettingsBase::DialogSettingsBase( const QList< QString > & watch, QWidget * parent )
	: DialogSettingsBase( parent )
{
	foreach( const QString & key, watch )
		m_stringWatch.insert( key, Settings::value( key ) );
}

DialogSettingsBase::DialogSettingsBase( const QList< QString > & watch,
		const QObject * receiver, const char * slot, QWidget * parent )
	: DialogSettingsBase( watch, parent )
{
	connect( this, SIGNAL( watchValueChanged( const QString & ) ),
			receiver, slot );
}

DialogSettingsBase::DialogSettingsBase( const QList< QString > & watch,
		std::function< void( const QString & ) > func, QWidget * parent )
	: DialogSettingsBase( watch, parent )
{
	connect( this, QOverload< const QString & >::of( &DialogSettingsBase::watchValueChanged ),
			func );
}

DialogSettingsBase::DialogSettingsBase( const QList< uint > & watch, QWidget * parent )
	: DialogSettingsBase( parent )
{
	foreach( uint key, watch )
		m_uintWatch.insert( key, Settings::value( key ) );
}

DialogSettingsBase::DialogSettingsBase( const QList< uint > & watch,
		const QObject * receiver, const char * slot, QWidget * parent )
	: DialogSettingsBase( watch, parent )
{
	connect( this, SIGNAL( watchValueChanged( uint ) ),
				receiver, slot );
}
DialogSettingsBase::DialogSettingsBase( const QList< uint > & watch,
		std::function< void( uint ) > func, QWidget * parent )
	: DialogSettingsBase( watch, parent )
{
	connect( this, QOverload< uint >::of( &DialogSettingsBase::watchValueChanged ),
			func );
}

DialogSettingsBase::~DialogSettingsBase()
{
	/// string watch
	auto s = m_stringWatch.constBegin();

	while ( s != m_stringWatch.constEnd() )
	{
		if ( s.value() != Settings::value( s.key() ) )
			emit watchValueChanged( s.key() );
		++s;
	}

	/// uint watch
	auto u = m_uintWatch.constBegin();

	while ( u != m_uintWatch.constEnd() )
	{
		if ( u.value() != Settings::value( u.key() ) )
			emit watchValueChanged( u.key() );
		++u;
	}

	Settings::setValue( STACK_INDEX, m_pager->currentRow(), GROUP );
}

void
DialogSettingsBase::createWidgets()
{
	m_pager = new QListWidget( this );
	m_pager->setViewMode( QListView::IconMode );
	m_pager->setIconSize( QSize( 64, 64 ) );
	m_pager->setMovement( QListView::Static );
	m_pager->setMaximumWidth( 128 );
	m_pager->setSpacing( 6 );
  m_pager->setItemAlignment( Qt::AlignCenter );

	m_stack = new QStackedWidget( this );

	connect( m_pager, SIGNAL( currentRowChanged( int ) ), m_stack, SLOT( setCurrentIndex( int ) ) );

	QHBoxLayout * layoutPager = new QHBoxLayout();

	layoutPager->addWidget( m_pager );
	layoutPager->addWidget( m_stack );

	/*
	QDialogButtonBox * buttonBox = new QDialogButtonBox( QDialogButtonBox::Ok | QDialogButtonBox::Cancel,
			Qt::Horizontal, this );
	connect( buttonBox, SIGNAL( accepted() ), SLOT( accept() ) );
	connect( buttonBox, SIGNAL( rejected() ), SLOT( reject() ) );
	*/

	QVBoxLayout * layout = new QVBoxLayout( this );

	layout->addLayout( layoutPager );
	layout->addLayout( new ButtonsLine( tr("Settings") ) );
	layout->addWidget( buttonBox() );
}

void
DialogSettingsBase::addSection( const QIcon & icon, const QString & caption, QWidget * widget )
{
	QListWidgetItem * item = new QListWidgetItem( m_pager );
	item->setIcon( icon );
	item->setText( caption );
	item->setTextAlignment( Qt::AlignHCenter );
	item->setFlags( Qt::ItemIsSelectable | Qt::ItemIsEnabled );

  QGridLayout * grid = qobject_cast< QGridLayout * >( widget->layout() );
  if ( grid )
    grid->setRowStretch( grid->rowCount(), 100 );

	m_stack->addWidget( widget );
}

void
DialogSettingsBase::addPage( const QIcon & icon, const QString & caption, QWidget * page )
{
  addSection( icon, caption, page );
}

void
DialogSettingsBase::show( int tab )
{
	m_pager->setCurrentRow( tab != -1 ? tab : Settings::value( STACK_INDEX, GROUP ).toInt() );

	QDialog::show();
}

void
DialogSettingsBase::showEvent( QShowEvent * event )
{
	// move to desktop center
  if ( qApp->screens().isEmpty() )
    return;

	const QRect desk = qApp->screens().first()->geometry();

	move( ( desk.width() - width() ) / 2, ( desk.height() - height() ) / 2 );

	QDialog::showEvent( event );
}

