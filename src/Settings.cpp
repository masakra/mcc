/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
 ┃                             ________________                                ┃
 ┃ _______ ________________    ___  /__(_)__  /______________ ____________  __ ┃
 ┃ __  __ `__ \  ___/  ___/    __  /__  /__  __ \_  ___/  __ `/_  ___/_  / / / ┃
 ┃ _  / / / / / /__ / /__      _  / _  / _  /_/ /  /   / /_/ /_  /   _  /_/ /  ┃
 ┃ /_/ /_/ /_/\___/ \___/      /_/  /_/  /_.___//_/    \__,_/ /_/    _\__, /   ┃
 ┃                                                                   /____/    ┃
 ┠─────────────────────────────────────────────────────────────────────────────┨
 ┃ Copyright © 2016, Sergey N Chursanov (masakra@mail.ru)                      ┃
 ┃ All rights reserved.                                                        ┃
 ┃                                 BSD license                                 ┃
 ┃                                                                             ┃
 ┃ Redistribution and use in source and binary forms, with or without          ┃
 ┃ modification, are permitted provided that the following conditions are met: ┃
 ┃                                                                             ┃
 ┃ 1. Redistributions of source code must retain the above copyright notice,   ┃
 ┃ this list of conditions and the following disclaimer.                       ┃
 ┃                                                                             ┃
 ┃ 2. Redistributions in binary form must reproduce the above copyright        ┃
 ┃ notice, this list of conditions and the following disclaimer in the         ┃
 ┃ documentation and/or other materials provided with the distribution.        ┃
 ┃                                                                             ┃
 ┃ THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" ┃
 ┃ AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE   ┃
 ┃ IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE  ┃
 ┃ ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE   ┃
 ┃ LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR         ┃
 ┃ CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF        ┃
 ┃ SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS    ┃
 ┃ INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN     ┃
 ┃ CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)     ┃
 ┃ ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE  ┃
 ┃ POSSIBILITY OF SUCH DAMAGE.                                                 ┃
 ┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

#include "Settings.h"

QSettings::Format Settings::g_format = QSettings::NativeFormat;

QSettings::Scope Settings::g_scope = QSettings::UserScope;


QHash< QString,		// group name
	QHash< QString,		// param name (in group)
		QVariant > > Settings::g_values;

QHash< uint,		// group name
	QHash< uint,		// param name
		QVariant >> Settings::g_uint_values;

QString
Settings::fullParamName( const QString & group, const QString & paramName )	// static inline
{
	return group.isEmpty() ? paramName : ( group + "/" + paramName );
}

QString
Settings::fullParamName( uint group, uint paramName )	// static inline
{
	return group == 0 ? QString::number( paramName ) :
		QString::number( group ) + "/" + QString::number( paramName );
}

void
Settings::setFormat( QSettings::Format format )		// static
{
	g_format = format;
}

QSettings::Format
Settings::format()		// static
{
	return g_format;
}

void
Settings::setScope( QSettings::Scope scope )		// static
{
	g_scope = scope;
}

QSettings::Scope
Settings::scope()		// static
{
	return g_scope;
}

bool
Settings::haveValue( const QString & group, const QString & paramName )	// private static
{
	return g_values.contains( group ) && g_values.value( group ).contains( paramName );
}

bool
Settings::haveValue( uint group, uint paramName )
{
	return g_uint_values.contains( group ) && g_uint_values.value( group ).contains( paramName );
}

const QVariant &
Settings::value( const QString & paramName, const QString & group, const QVariant & def_value )	// static
{
	if ( ! haveValue( group, paramName ) ) {
		QSETTINGS_s
		g_values[ group ][ paramName ] = s.value( fullParamName( group, paramName ), def_value );
	}

	return g_values[ group ][ paramName ];
}

void
Settings::setValue( const QString & paramName, const QVariant & newValue,
		const QString & group )			// static
{
	if ( haveValue( group, paramName ) &&
			g_values[ group ][ paramName ] == newValue )
		return;

	QSETTINGS_s

	s.setValue( fullParamName( group, paramName ), newValue );
	g_values[ group ][ paramName ] = newValue;
}

const QVariant &
Settings::value( uint paramName, uint group, const QVariant & def_value )
{
	if ( ! haveValue( group, paramName ) ) {
		QSETTINGS_s
		g_uint_values[ group ][ paramName ] = s.value( fullParamName( group, paramName ), def_value );
	}

	return g_uint_values[ group ][ paramName ];
}

void
Settings::setValue( uint paramName, const QVariant & newValue, uint group )
{
	if ( haveValue( group, paramName ) &&
			g_uint_values[ group ][ paramName ] == newValue )
		return;

	QSETTINGS_s

	s.setValue( fullParamName( group, paramName ), newValue );
	g_uint_values[ group ][ paramName ] = newValue;
}

