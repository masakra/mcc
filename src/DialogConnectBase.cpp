/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
 ┃                             ________________                                ┃
 ┃ _______ ________________    ___  /__(_)__  /______________ ____________  __ ┃
 ┃ __  __ `__ \  ___/  ___/    __  /__  /__  __ \_  ___/  __ `/_  ___/_  / / / ┃
 ┃ _  / / / / / /__ / /__      _  / _  / _  /_/ /  /   / /_/ /_  /   _  /_/ /  ┃
 ┃ /_/ /_/ /_/\___/ \___/      /_/  /_/  /_.___//_/    \__,_/ /_/    _\__, /   ┃
 ┃                                                                   /____/    ┃
 ┠─────────────────────────────────────────────────────────────────────────────┨
 ┃ Copyright © 2016, Sergey N Chursanov (masakra@mail.ru)                      ┃
 ┃ All rights reserved.                                                        ┃
 ┃                                 BSD license                                 ┃
 ┃                                                                             ┃
 ┃ Redistribution and use in source and binary forms, with or without          ┃
 ┃ modification, are permitted provided that the following conditions are met: ┃
 ┃                                                                             ┃
 ┃ 1. Redistributions of source code must retain the above copyright notice,   ┃
 ┃ this list of conditions and the following disclaimer.                       ┃
 ┃                                                                             ┃
 ┃ 2. Redistributions in binary form must reproduce the above copyright        ┃
 ┃ notice, this list of conditions and the following disclaimer in the         ┃
 ┃ documentation and/or other materials provided with the distribution.        ┃
 ┃                                                                             ┃
 ┃ THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" ┃
 ┃ AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE   ┃
 ┃ IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE  ┃
 ┃ ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE   ┃
 ┃ LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR         ┃
 ┃ CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF        ┃
 ┃ SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS    ┃
 ┃ INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN     ┃
 ┃ CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)     ┃
 ┃ ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE  ┃
 ┃ POSSIBILITY OF SUCH DAMAGE.                                                 ┃
 ┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

#include "DialogConnectBase.h"

#include <QtSql>
#include <QtWidgets>

#include "Label.h"
#include "Settings.h"

#define MARGIN 20

#define GROUP DIALOG_CONNECT_BASE_GROUP	// from header file
#define USERNAME "username"
#define PASSWORD "password"
#define HOST "host"
#define PORT "port"
#define DATABASE "database"
#define AUTOLOGIN DIALOG_CONNECT_BASE_AUTOLOGIN // from header file

DialogConnectBase::DialogConnectBase( QWidget * parent, const QString & driver, const QString & background )
	: QDialog( parent ), m_pixmapBackground( background ), m_driver( driver ), m_autologin( true ),
	m_versionPos( 290, 20 )
{
	setWindowFlags( Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint );

	setAttribute( Qt::WA_DeleteOnClose );

	resize( 640, 300 );

  if ( ! qApp->screens().isEmpty() ) {
    const QSize offset = ( qApp->screens().first()->geometry().size() - size() ) / 2;
    move( offset.width(), offset.height() );
  }

	setWindowOpacity( .98 );

	createWidgets();

	if ( QWidget * empty = load() ) {
		m_buttonServer->toggle();
		empty->setFocus();
	} else if ( m_editUsername->text().isEmpty() )
		m_editUsername->setFocus();
	else
		m_editPassword->setFocus();

	setInfo();

	if ( m_autologin && m_checkAutologin->isChecked() ) {
		addMessage( tr("Autologin") );
		connectToDatabase();
	}
}

void
DialogConnectBase::createWidgets()
{
	m_editUsername = new QLineEdit;
	connect( m_editUsername, SIGNAL( textChanged( const QString & ) ), SLOT( checkFields( const QString & ) ) );

	m_editPassword = new QLineEdit;
	m_editPassword->setEchoMode( QLineEdit::Password );

	m_checkAutologin = new QCheckBox( tr("auto&login"), this );

	m_labelInfo = new QLabel( this );
	m_labelInfo->setEnabled( false );

	m_buttonServer = new QToolButton;
	m_buttonServer->setToolButtonStyle( Qt::ToolButtonIconOnly );
	m_buttonServer->setIcon( QIcon::fromTheme("network-server", QIcon(":/avcc_server_database_16.png") ) );
	m_buttonServer->setCheckable( true );
	connect( m_buttonServer, SIGNAL( toggled( bool ) ), SLOT( toggleServerFieldsVisible( bool ) ) );

	m_editHost = new QLineEdit;
	m_editHost->hide();
	m_labelHost = new Label( tr("&Host"), m_editHost );
	m_labelHost->hide();
	connect( m_editHost, SIGNAL( textChanged( const QString & ) ), SLOT( checkFields( const QString & ) ) );

	m_editPort = new QLineEdit;
	m_editPort->setValidator( new QIntValidator( 1, 65536, this ) );
	m_editPort->hide();
	m_labelPort = new Label( tr("&Port"), m_editPort );
	m_labelPort->hide();
	connect( m_editPort, SIGNAL( textChanged( const QString & ) ), SLOT( checkFields( const QString & ) ) );

	m_editDatabase = new QLineEdit;
	m_editDatabase->hide();
	m_labelDatabase = new Label( tr("&Database"), m_editDatabase );
	m_labelDatabase->hide();
	connect( m_editDatabase, SIGNAL( textChanged( const QString & ) ), SLOT( checkFields( const QString & ) ) );

	// кнопки
	auto button = []( const QString & caption )->QPushButton *
	{
		QPushButton * b = new QPushButton( caption );
		b->setMinimumSize( 150, 32 );
		return b;
	};
	m_buttonConnect = button( tr("Connect") );
	m_buttonConnect->setDefault( true );
	connect( m_buttonConnect, SIGNAL( clicked() ), SLOT( connectToDatabase() ) );

	QPushButton * buttonClose = button( tr("Close") );
	connect( buttonClose, SIGNAL( clicked() ), SLOT( reject() ) );

	QHBoxLayout * buttonsLayout = new QHBoxLayout;
	buttonsLayout->setSpacing( 20 );
	buttonsLayout->addStretch();
	buttonsLayout->addWidget( m_buttonConnect );
	buttonsLayout->addWidget( buttonClose );

	QHBoxLayout * layoutButtonServer = new QHBoxLayout;

	layoutButtonServer->addStretch();
	layoutButtonServer->addWidget( m_labelInfo );
	layoutButtonServer->addWidget( m_buttonServer );

	QGridLayout * gridLayout = new QGridLayout,
				* gridUserLayout = new QGridLayout,
				* gridServerLayout = new QGridLayout;

	gridUserLayout->setRowStretch( 0, 100 );
	gridUserLayout->setColumnStretch( 0, 100 );
	gridUserLayout->addWidget( new Label( tr("&Username"), m_editUsername ), 1, 0, Qt::AlignRight );
	gridUserLayout->addWidget( m_editUsername, 1, 1 );
	gridUserLayout->addWidget( new Label( tr("&Password"), m_editPassword ), 2, 0, Qt::AlignRight );
	gridUserLayout->addWidget( m_editPassword, 2, 1 );
	gridUserLayout->addWidget( m_checkAutologin, 3, 0, 1, 2, Qt::AlignRight );

	gridServerLayout->addLayout( layoutButtonServer, 0, 0, 1, 2, Qt::AlignRight );
	gridServerLayout->addWidget( m_labelHost, 1, 0, Qt::AlignRight );
	gridServerLayout->addWidget( m_editHost, 1, 1 );
	gridServerLayout->addWidget( m_labelPort, 2, 0, Qt::AlignRight );
	gridServerLayout->addWidget( m_editPort, 2, 1 );
	gridServerLayout->addWidget( m_labelDatabase, 3, 0, Qt::AlignRight );
	gridServerLayout->addWidget( m_editDatabase, 3, 1 );
	gridServerLayout->setRowStretch( 4, 100 );

	gridLayout->addLayout( gridUserLayout, 0, 1 );
	gridLayout->addLayout( gridServerLayout, 1, 1 );
	gridLayout->addLayout( buttonsLayout, 2, 1 );
	gridLayout->setRowStretch( 0, 100 );
	gridLayout->setRowStretch( 1, 130 );
	gridLayout->setRowStretch( 2, 10 );
	gridLayout->setColumnStretch( 0, 10 );

	QHBoxLayout * layout = new QHBoxLayout( this );
	layout->setMargin( MARGIN );
	layout->addStretch();
	layout->addLayout( gridLayout );
}

void
DialogConnectBase::paintEvent( QPaintEvent * event )
{
	QPainter painter( this );
	painter.drawPixmap( 0, 0, m_pixmapBackground );

	// version
	if ( ! qApp->applicationVersion().isEmpty() ) {
		painter.save();
		painter.setPen( m_colorVersion );
		QFont f( font() );
		f.setPointSizeF( 7.5 );
		painter.setFont( f );
		painter.drawText( m_versionPos, tr("version %1").arg( qApp->applicationVersion() ) );
		painter.restore();
	}

	// messages

	QFontMetrics fm( painter.font() );
	const int rows = ( height() - MARGIN - MARGIN ) / fm.height();

	for ( int i = 0; i < m_messages.count() && i < rows; ++i ) {
		if ( i ) {
			painter.setPen( m_colorMessage );
			painter.setOpacity( 1 - i / ( qreal ) rows );
		} else
			painter.setPen( m_colorLastMessage );

		painter.drawText( MARGIN, height() - MARGIN - ( fm.height() * i ), m_messages.at( i ) );
	}

	QDialog::paintEvent( event );
}

void
DialogConnectBase::toggleServerFieldsVisible( bool e )
{
	m_labelHost->setVisible( e );
	m_editHost->setVisible( e );
	m_labelPort->setVisible( e );
	m_editPort->setVisible( e );
	m_labelDatabase->setVisible( e );
	m_editDatabase->setVisible( e );

	m_labelInfo->setVisible( ! e );

	if ( ! e )
		setInfo();
}

void
DialogConnectBase::setInfo()
{
	m_labelInfo->setText( tr("%1:%2:%3")
			.arg( m_editHost->text() )
			.arg( m_editPort->text() )
			.arg( m_editDatabase->text() ) );
}

QWidget *
DialogConnectBase::load()
{
	m_editUsername->setText( Settings::value( USERNAME, GROUP ).toString() );
	m_editHost->setText( Settings::value( HOST, GROUP, "localhost").toString() );
	m_editPort->setText( Settings::value( PORT, GROUP, "5432").toString() );
	m_editDatabase->setText( Settings::value( DATABASE, GROUP ).toString() );
	m_checkAutologin->setChecked( Settings::value( AUTOLOGIN, GROUP, false ).toBool() );

	if ( m_checkAutologin->isChecked() )
		m_editPassword->setText( Settings::value( PASSWORD, GROUP ).toString() );


	if ( m_editHost->text().isEmpty() )
		return m_editHost;

	if ( m_editPort->text().isEmpty() )
		return m_editPort;

	if ( m_editDatabase->text().isEmpty() )
		return m_editDatabase;

	return nullptr;
}

void
DialogConnectBase::save() const
{
	Settings::setValue( USERNAME, m_editUsername->text(), GROUP );
	Settings::setValue( HOST, m_editHost->text(), GROUP );
	Settings::setValue( PORT, m_editPort->text(), GROUP );
	Settings::setValue( DATABASE, m_editDatabase->text(), GROUP );

	Settings::setValue( AUTOLOGIN, m_autologin && m_checkAutologin->isChecked(), GROUP );

	if ( m_checkAutologin->isChecked() )
		Settings::setValue( PASSWORD, m_editPassword->text(), GROUP );
	else
		Settings::setValue( PASSWORD, "", GROUP );
}

void
DialogConnectBase::addMessage( const QString & message )	// public slot
{
	m_messages.prepend( tr("[%1] %2")
			.arg( QTime::currentTime().toString("hh:mm:ss") )
			.arg( message ) );
	update();
}

void
DialogConnectBase::connectToDatabase()	// private slot
{
	QSqlDatabase db = QSqlDatabase::database();

	if ( ! db.isValid() )
		db = QSqlDatabase::addDatabase( m_driver );

	if ( m_driver == "QPSQL" ) {
		db.setHostName( m_editHost->text() );
		db.setPort( m_editPort->text().toInt() );
		db.setDatabaseName( m_editDatabase->text() );
		db.setUserName( m_editUsername->text() );
		db.setPassword( m_editPassword->text() );
		db.setConnectOptions( QString("application_name='%1 v.%2'")
				.arg( qApp->applicationName().isEmpty() ? "undefined" : qApp->applicationName() )
				.arg( qApp->applicationVersion().isEmpty() ? "undefined" : qApp->applicationVersion() ) );
	} else if ( m_driver == "QODBC" ) {
#ifdef Q_OS_WIN
		const QString cs = QString("Driver={sql server}; server=%1; port=%2; database=%3; uid=%4; pwd=%5;")
#endif

#ifdef Q_OS_UNIX
		const QString cs = QString("DRIVER={FreeTDS};SERVER=%1;TDS_VERSION=8.0;PORT=%2;DATABASE=%3;UID=%4;PWD=%5;")
#endif
								.arg( m_editHost->text() )
								.arg( m_editPort->text() )
								.arg( m_editDatabase->text() )
								.arg( m_editUsername->text() )
								.arg( m_editPassword->text() );
		db.setDatabaseName( cs );
	}

	if ( db.open() ) {
		save();
		addMessage( tr("connected") );
		QTimer::singleShot( 200, this, SLOT( accept() ) );
	} else {
		m_editPassword->selectAll();
		m_editPassword->setFocus();
		addMessage( db.lastError().text() );
	}
}

void
DialogConnectBase::checkFields( const QString & text )
{
	bool e = text.isEmpty() ? true : (
			m_editUsername->text().isEmpty() ||
			m_editHost->text().isEmpty() ||
			m_editPort->text().isEmpty() ||
			m_editDatabase->text().isEmpty() );

	m_buttonConnect->setEnabled( ! e );
}

void
DialogConnectBase::setVersionPos( int x, int y )
{
	m_versionPos = QPoint( x, y );
	update();
}

void
DialogConnectBase::enableAutologin( bool e )
{
	m_autologin = e;
	m_checkAutologin->setEnabled( e );
}

