<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>ColorDisplay</name>
    <message>
        <location filename="../src/ColorDisplay.cpp" line="131"/>
        <source>Copy color</source>
        <translation>Копировать цвет</translation>
    </message>
    <message>
        <location filename="../src/ColorDisplay.cpp" line="140"/>
        <source>Paste color &lt;%1&gt;</source>
        <translation>Вставить цвет &lt;%1&gt;</translation>
    </message>
</context>
<context>
    <name>DialogConnectBase</name>
    <message>
        <location filename="../src/DialogConnectBase.cpp" line="84"/>
        <source>Autologin</source>
        <translation>Автоматический вход</translation>
    </message>
    <message>
        <location filename="../src/DialogConnectBase.cpp" line="98"/>
        <source>auto&amp;login</source>
        <translation>автоматический &amp;вход</translation>
    </message>
    <message>
        <location filename="../src/DialogConnectBase.cpp" line="111"/>
        <source>&amp;Host</source>
        <translation>&amp;Сервер</translation>
    </message>
    <message>
        <location filename="../src/DialogConnectBase.cpp" line="118"/>
        <source>&amp;Port</source>
        <translation>&amp;Порт</translation>
    </message>
    <message>
        <location filename="../src/DialogConnectBase.cpp" line="124"/>
        <source>&amp;Database</source>
        <translation>База &amp;данных</translation>
    </message>
    <message>
        <location filename="../src/DialogConnectBase.cpp" line="135"/>
        <source>Connect</source>
        <translation>Соединиться</translation>
    </message>
    <message>
        <location filename="../src/DialogConnectBase.cpp" line="139"/>
        <source>Close</source>
        <translation>Закрыть</translation>
    </message>
    <message>
        <location filename="../src/DialogConnectBase.cpp" line="160"/>
        <source>&amp;Username</source>
        <translation>&amp;Пользователь</translation>
    </message>
    <message>
        <location filename="../src/DialogConnectBase.cpp" line="162"/>
        <source>&amp;Password</source>
        <translation>&amp;Пароль</translation>
    </message>
    <message>
        <location filename="../src/DialogConnectBase.cpp" line="202"/>
        <source>version %1</source>
        <translation>версия %1</translation>
    </message>
    <message>
        <location filename="../src/DialogConnectBase.cpp" line="243"/>
        <source>%1:%2:%3</source>
        <translation>%1:%2:%3</translation>
    </message>
    <message>
        <location filename="../src/DialogConnectBase.cpp" line="293"/>
        <source>[%1] %2</source>
        <translation>[%1] %2</translation>
    </message>
    <message>
        <location filename="../src/DialogConnectBase.cpp" line="334"/>
        <source>connected</source>
        <translation>соединение установлено</translation>
    </message>
</context>
<context>
    <name>DialogSettingsBase</name>
    <message>
        <location filename="../src/DialogSettingsBase.cpp" line="48"/>
        <location filename="../src/DialogSettingsBase.cpp" line="156"/>
        <source>Settings</source>
        <translation>Настройки</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/Info.cpp" line="80"/>
        <source>WARNING: </source>
        <translation>ПРЕДУПРЕЖДЕНИЕ: </translation>
    </message>
    <message>
        <location filename="../src/Info.cpp" line="83"/>
        <source>ERROR: </source>
        <translation>ОШИБКА: </translation>
    </message>
    <message>
        <location filename="../src/FSDialog.cpp" line="47"/>
        <source>;;All files (* *.*)</source>
        <translation>;;Все файлы (* *.*)</translation>
    </message>
</context>
</TS>
